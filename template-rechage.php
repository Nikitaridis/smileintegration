<?php
/**
 * The template for displaying recharge page
 * Template Name: Recharge
 * @package storefront
 */

if (!isset($_POST['recharge-product-id'])) {
    wp_redirect(PageType::getPageUrl('data-bundles'));
    exit;
}

$product_id = $_POST['recharge-product-id'];
$product = wc_get_product($product_id);

if (is_post_not_empty('recharge-email') && is_post_not_empty('recharge-service') || (is_post_not_empty('recharge-msisdn') && is_post_not_empty('recharge-service'))) {

    $recharge_service = json_decode(stripslashes($_POST['recharge-service']));

    $customer_data = '';
    if (is_post_not_empty('recharge-msisdn')) {
        $customer_data = apply_filters('smile_communication_api_get_customer_by_email_with_services', $_POST['recharge-msisdn']);
    } else if (is_post_not_empty('recharge-email')) {
        $customer_data = apply_filters('smile_communication_api_get_customer_by_email_with_services', $_POST['recharge-email']);
    }

    if (!property_exists($customer_data, 'error')) {

        $got_all_data = false;
        $customer_id = -1;
        $account_id = -1;
        $recipient_organisation_id = 0;

        if (property_exists($customer_data, 'customer')) {
            $customer = $customer_data->customer;
            $customer_id = $customer->customerId;
            if (property_exists($customer, 'products')) {
                $products = $customer->products;
                foreach ($products as $productService) {
                    //$recipient_organisation_id = $productService->organisationId;
                    if (property_exists($productService, 'services')) {
                        $services = $productService->services;
                        foreach ($services as $service) {
                            if ($recharge_service->accountHash == md5($service->accountId . $service->serviceICCID) && $service->status !== "TD") {
                                $account_id = $service->accountId;
                                $customer_fullname = $customer->firstName . ' ' . (strlen(trim($customer->middleName)) ? ($customer->middleName . ' ') : '') . $customer->lastName;
                                $customer_firstname = $customer->firstName;
                                $customer_lastname = $customer->lastName;
                                $customer_email = $customer->emailAddress;
                                $customer_msisdn = $customer->msisdn;
                                $recipient_organisation_id = $productService->organisationId;
                                $got_all_data = true;
                                break 2;
                            }
                        }
                    }
                }
            }
        }

        if ($got_all_data) {

            $order = wc_create_order();
            $order->add_product($product);
            $order->set_address(
                array(
                    'email' => $customer_email,
                    'first_name' => $customer_firstname,
                    'last_name' => $customer_lastname,
                    'full_name' => $customer_fullname
                )
            );
            $order->calculate_totals();

            update_post_meta($order->get_id(), 'smile_order_customer_id', $customer_id);
            update_post_meta($order->get_id(), 'smile_recharge_account_id', $account_id);
            update_post_meta($order->get_id(), 'smile_recharge_product_instance_id', $recharge_service->productInstanceId);
            update_post_meta($order->get_id(), '_smile_is_recharge', 1);
            update_post_meta($order->get_id(), 'smile_recharge_recipient_organisation_id', $recipient_organisation_id);

            update_post_meta($order->get_id(), '_payment_method', 'wcspg_smile_payment');
            update_post_meta($order->get_id(), '_payment_method_title', RECHARGE_PAYMENT_METHOD_TITLE);

//            update_post_meta($order->id, '_payment_method', 'cod');
//            update_post_meta($order->id, '_payment_method_title', 'Cash on delivery');

            //Session gave problems when ordering normal product after recharge
            // Set shipping method to delivery (else pay by card doesn't work)
//            WC()->session->set('chosen_shipping_methods', array('local_delivery'));
            // Store Order ID in session so it can be re-used after payment failure
//            WC()->session->order_awaiting_payment = $order->id;

            // Process Payment
            $available_gateways = WC()->payment_gateways->get_available_payment_gateways();
            $result = $available_gateways['wcspg_smile_payment']->process_payment($order->get_id());
//            $result = $available_gateways['cod']->process_payment($order->id);

            // Redirect to success/confirmation/payment page
            if ($result['result'] == 'success') {

                $result = apply_filters('woocommerce_payment_successful_result', $result, $order->get_id());

                wp_redirect($result['redirect']);
                exit;
            }
        }
    }
}
wp_enqueue_script('smile-recharge-script', get_stylesheet_directory_uri() . '/js/recharge.js', array(), SMILE_VERSION, true);
wp_localize_script('smile-recharge-script', 'RechargeConfig',
    array(
        'unrecognizedEmailText' => RECHARGE_UNRECOGNIZED_EMAIL_TEXT
    )
);
?>

<?php get_header(); ?>

<?php
// Page post object
$Page = get_post();

// Databundle
$dataBundleHTML = '';
$dataBundleGB = $product->get_attribute('pa_data-bundle-gb');
$dataBundleMB = $product->get_attribute('pa_data-bundle-mb');
$dataBundleUnlimited = $product->get_attribute('pa_data-bundle-unlimited');
if (!empty($dataBundleGB)) {
    $dataBundleHTML = $dataBundleGB . '<span class="data-bundle-size">GB</span>';
} elseif (!empty($dataBundleMB)) {
    $dataBundleHTML = $dataBundleMB . '<span class="data-bundle-size">MB</span>';
} elseif (!empty($dataBundleUnlimited)) {
    $dataBundleHTML = '<span class="text">' . $dataBundleUnlimited . '</span>';
}

$circleColor = $product->get_attribute('pa_data-bundle-color');
$outOfBundle = $product->get_attribute('pa_data-bundle-out-of-bundle');
?>

    <section id="recharge" class="fill-screen">
        <div class="row">
            <div class="twelve columns section-title section-text-center">
                <h1><?php echo get_post()->post_title; ?></h1>
                <?php echo get_the_subtitle(); ?>
            </div>

            <div class="recharge-block">
                <div class="data-bundle-row six columns">
                    <div class="five columns data-bundle-column">
                        <div class="circle <?php echo $circleColor; ?>">
                            <span class="circle-back"></span>
                            <span class="data-bundle"><?php echo $dataBundleHTML; ?></span>
                        </div>
                    </div>
                    <div class="seven columns data-bundle-column">
                        <span class="column-title">Includes:</span>
                        <ul>
                            <?php if (!empty($product->get_attribute('pa_data-bundle-limit'))): ?>
                                <li><?php echo $product->get_attribute('pa_data-bundle-limit'); ?></li>
                            <?php endif; ?>
                            <li><?php echo $product->get_attribute('pa_data-bundle-validity'); ?> validity</li>
                            <?php if (!empty($outOfBundle)): ?>
                                <li><?php echo $outOfBundle; ?>/GB Out of Bundle</li>
                            <?php endif; ?>
                            <?php echo(empty($product->get_attribute('pa_data-bundle-text')) ? '' : ('<li>' . $product->get_attribute('pa_data-bundle-text') . '</li>')); ?>
                            <?php echo(empty($product->get_attribute('pa_data-bundle-text2')) ? '' : ('<li>' . $product->get_attribute('pa_data-bundle-text2') . '</li>')); ?>
                        </ul>
                        <div class="price"><span class="column-title">Price:</span><?php echo $product->get_price_html(); ?>
                        </div>
                    </div>
                </div>

                <div class="recharge-form-parent six columns">
                    <form id="recharge-form" name="recharge-form" method="post">
                        <div class="error" style="display: none"></div>
                        <input type="hidden" name="recharge-product-id" value="<?php echo $product_id; ?>"/>
                        <label for="recharge-email">Enter registered Smile email address</label>
                        <input type="email" name="recharge-email" autofocus/>
                        <label for="recharge-msisdn">Enter registered Smile MSISDN</label>
                        <input type="text" name="recharge-msisdn" autofocus/>
                        <a class="check-email">Show my accounts</a>
                        <label for="recharge-account">Select your Smile account:</label>
                        <select name="recharge-account" disabled></select>
                        <label for="recharge-service">Select Smile SIM card:</label>
                        <select name="recharge-service" disabled></select>
                        <button id="recharge-submit" type="submit" disabled>SUBMIT AND GO TO PAYMENT</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div id="fullscreen-loader">
        <div class="spinner"></div>
        <div class="message">Loading customer data</div>
    </div>

<?php get_footer(); ?>