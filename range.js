jQuery(document).ready(function($) {

    var checkEmailButton = $('a.check-email');
    var emailInput = $('input[name="recharge-email"]');
    var accountsSelect = $('select[name="recharge-account"]');
    var serviceSelect = $('select[name="recharge-service"]');
    var rechargeMsisdn = $('input[name="recharge-msisdn"]');
    var rechargeButton = $('button#recharge-submit');
    var error = $('#recharge-form .error');

    checkEmailButton.on('click', function(e) {
        e.preventDefault();

        //getUserInformation();  TODO changeme
        getUserInformationTestGet();
        // getUserInformationTestPost();
    });

    emailInput.keypress(function(e) {
        if(e.which == 13) {
            e.preventDefault();
            emailInput.blur();
            getUserInformation();
            return false;
        }
    });

    accountsSelect.on('change', function() {
        addServicesToSelect();
    });


    function getUserInformationTestPost() {

        var searchCriteria = null;
        if(emailInput.val()) {
            searchCriteria = emailInput.val();
        } else if (rechargeMsisdn.val()) {
            searchCriteria = rechargeMsisdn.val();
        } else {
            showRechargeError("please enter valid MSISDN or email address")
        }

        alert('getUserInformation'+searchCriteria);
        $.ajax({
            url: WebsiteConfig.ajaxurl,
            method: 'POST',
            data: {
                action: 'getRechargeAccounts',
                email: searchCriteria.val()
            },
            success: function (data) {alert('Success  '+data)},
            error: function (errorThrown){alert('error'+errorThrown)}});
    }



    function getUserInformationTestGet() {

        var searchCriteria = null;
        if(emailInput.val()) {
            searchCriteria = emailInput.val();
        } else if (rechargeMsisdn.val()) {
            searchCriteria = rechargeMsisdn.val();
        } else {
            showRechargeError("please enter valid MSISDN or email address")
        }

        alert('getUserInformation'+searchCriteria);
        //https://smile.com.ng/acc/
        // curl https://www.smile.com.ng/sra/customers/07020149320?by=smilevoice
        $.ajax({
            url: 'https://www.smile.com.ng/sra/customers/07020149320?by=smilevoice',
            method: 'GET',
            success: function (data) {alert('Success')},
            error: function (errorThrown){alert('error'+errorThrown)}});
    }


    function getUserInformation() {
        var searchCriteria = null;
        if(emailInput.val()) {
            searchCriteria = emailInput.val();
        } else if (rechargeMsisdn.val()) {
            searchCriteria = rechargeMsisdn.val();
        } else {
            showRechargeError("please enter valid MSISDN or email address")
        }

        $('#fullscreen-loader').css('display', 'block');
        alert('getUserInformation'+searchCriteria);
        $.ajax({
            url: WebsiteConfig.ajaxurl,
            method: 'POST',
            data: {
                action: 'getRechargeAccounts',
                email: searchCriteria.val()
            },
            success: function (data) {
                $('#fullscreen-loader').css('display', 'none');
                var parsedData = JSON.parse(data);
                if (typeof parsedData.error === "undefined") {
                    error.hide();
                    if (parsedData.data !== "undefined") {
                        accountsSelect.prop('disabled', false);
                        serviceSelect.prop('disabled', false);
                        rechargeButton.prop('disabled', false);

                        accountsSelect.find('option').remove().end();

                        $.each(parsedData.data, function(index, value) {

                            var serviceArray = [];
                            $.each(value, function(serviceIndex, serviceValue) {
                                var tempServiceObj = {};
                                tempServiceObj.serviceICCID = serviceIndex;
                                tempServiceObj.accountHash = serviceValue.accountHash;
                                if(serviceValue.productFriendlyName) {
                                    tempServiceObj.productFriendlyName = serviceValue.productFriendlyName;
                                }
                                tempServiceObj.productInstanceId = serviceValue.productInstanceId;
                                serviceArray.push(tempServiceObj);
                            });
                            var optionObj = $("<option/>").attr('data-services',JSON.stringify(serviceArray)).html(index);
                            accountsSelect.append(optionObj);
                        });

                        addServicesToSelect();
                    } else {
                        showRechargeError('Parsed data from API is undefined');
                    }
                } else {
                    showRechargeError(parsedData.error);
                }
            },
            error: function (errorThrown) {
                $('#fullscreen-loader').css('display', 'none');
                showRechargeError('issue when calling API with email'+errorThrown)
            }
        });
    }

    function addServicesToSelect() {
        serviceSelect.find('option').remove().end();

        var data = accountsSelect.find(':selected').data('services');
        $.each(data, function (index, value) {
            var tempServiceObj = {};
            tempServiceObj.accountHash = value.accountHash;
            tempServiceObj.productInstanceId = value.productInstanceId;
            var optionValue = JSON.stringify(tempServiceObj);
            var optionText = value.serviceICCID + ((value.productFriendlyName) ? (" (" + value.productFriendlyName + ")") : "");
            serviceSelect.append($("<option/>").html(optionText).val(optionValue));
        });
    }


    function showRechargeError(errorString) {

        var windowOverlayContent = $('.window-overlay-content');
        $('body').css('overflow', 'hidden');
        $('.window-overlay').addClass('window-open');
        windowOverlayContent.show();

        windowOverlayContent.addClass('window-overlay-centered-in-screen');
        windowOverlayContent.find('.recharge-error-popup').remove();
        windowOverlayContent.append('' +
            '<div class="recharge-error-popup"><h3>'+errorString+'</h3>' + RechargeConfig.unrecognizedEmailText + '</div>');

        windowOverlayContent.css("top", Math.max(0, (($(window).height() - windowOverlayContent.outerHeight()) / 2) +
            $(window).scrollTop()) + "px");
        windowOverlayContent.css("left", Math.max(0, (($(window).width() - windowOverlayContent.outerWidth()) / 2) +
            $(window).scrollLeft()) + "px");

        accountsSelect.find('option').remove().end();
        serviceSelect.find('option').remove().end();
        accountsSelect.prop('disabled', true);
        serviceSelect.prop('disabled', true);
        rechargeButton.prop('disabled', true);

        //error.show();
        //error.empty();
        //error.append(errorString);
    }
});